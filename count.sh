#!/bin/bash
: ' Counts to a number
    I used this with the `time` command to compare the execution time between different devices
    Kinda like a pseudo-benchmark
  '
let "a = 0"
while [ $a -lt 1000000 ]
do
	echo "$a"
	let "a += 1"
done
