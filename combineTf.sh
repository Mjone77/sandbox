#!/bin/bash

:' Puts all .tf files into one file
 '

for file in ./*
do
    if [[ $file =~ \./.*\.tf$ ]]
    then
        cat $file >> all.tf
    fi
done
